import 'should';
import * as fs from 'fs';
import * as path from 'path';

import { run } from '../server';
import { rmdir } from './helpers';

const TESTS_DIR = '/tmp/kresus-tests';
const DB_PATH = path.join(TESTS_DIR, 'kresus-export-cozydb-db-path');

// Thanks stackoverflow!

describe('properly run a migration from an empty database', async () => {
    before(() => {
        if (fs.existsSync(TESTS_DIR)) {
            rmdir(TESTS_DIR);
        }
        fs.mkdirSync(TESTS_DIR);
    });

    it('should run properly', async () => {
        let options = {
            dbName: DB_PATH
        };
        await run(options);
    });
});
