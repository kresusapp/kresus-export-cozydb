import 'should';
import * as fs from 'fs';
import * as path from 'path';

import { run } from '../server';
import { rmdir } from './helpers';

const TESTS_DIR = '/tmp/kresus-tests';
const DB_PATH = path.join(TESTS_DIR, 'undefined-access-fields');

const options = {
    dbName: DB_PATH
};

describe('run a migration and remove incorrect custom fields', async () => {
    before(() => {
        if (fs.existsSync(TESTS_DIR)) {
            rmdir(TESTS_DIR);
        }
        fs.mkdirSync(TESTS_DIR);
    });

    it('should run properly', async () => {
        // Run once to configure the DB.
        const emptyDb = await run(options);
        emptyDb.accesses.length.should.equal(0);

        const validField = { name: 'name', value: 'value' };
        const access = {
            vendorId: 'toto',
            login: 'ZeHiro',
            password: 'test',
            fields: [
                { name: 'undefinedValue' },
                { value: 'undefinedName' },
                { name: 'emptyValue', value: '' },
                { name: '', value: 'emptyName' },
                validField
            ]
        };

        // The db needs to be initialized before importing a model.
        const Accesses = require('../server/models/accesses');

        await Accesses.create(0, access);
        const exportWithAccess = await run(options);
        exportWithAccess.accesses.length.should.equal(1);
        const { fields } = exportWithAccess.accesses[0];

        should.exist(fields);
        fields.should.be.instanceOf(Array);
        fields.length.should.equal(1);
        fields.should.deepEqual([validField]);
    });
});
