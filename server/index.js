import cozydb from 'cozydb';
import path from 'path';
import PouchDB from 'pouchdb';
import * as fs from 'fs';

function configureCozyDB(options) {
    return new Promise((resolve, reject) => {
        cozydb.configure(options, null, err => {
            if (err) {
                return reject(err);
            }
            resolve();
        });
    });
}

async function runHelper(options, cleanPassword) {
    options.name = 'Kresus';
    options.root = options.root || path.join(__dirname, '..');

    // eslint-disable-next-line camelcase
    options.db = new PouchDB(options.dbName);
    options.modelsPath = path.join(__dirname, 'models', 'pouch');

    await configureCozyDB(options);

    let all = require('./all');
    let jsonOutput = await all.getAllData(0, /* isExport */ true, cleanPassword);

    return jsonOutput;
}

async function run(options) {
    return await runHelper(options, /* cleanPassword */ false);
}

async function main(options, outputFile) {
    let jsonOutput = await runHelper(options, /* cleanPassword */ true);
    let textOutput = JSON.stringify(jsonOutput, null, 2);
    fs.writeFileSync(outputFile, textOutput);
}

module.exports = {
    main,
    run
};
