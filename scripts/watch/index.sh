#!/bin/bash
set -e

yarn run build:server

concurrently -k \
    "yarn run -- onchange './shared/*.json' -- cp '{{changed}}' ./build/server/shared" \
    "yarn run babel --skip-initial-build ./server/ -d ./build/server -w"
