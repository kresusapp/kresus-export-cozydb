/* eslint no-console: 0 */

import moment from 'moment';

import ACCOUNT_TYPES from './account-types.json';
import OPERATION_TYPES from './operation-types.json';

export function maybeHas(obj, prop) {
    return obj && obj.hasOwnProperty(prop);
}

// Example: 02/25/2019
const toShortString = date => moment(date).format('L');

// Example: February 25, 2019
const toDayString = date => moment(date).format('LL');

// Example: Monday, February 25, 2019 10:04 PM
const toLongString = date => moment(date).format('LLLL');

// Example: 5 minutes ago
const fromNow = date => moment(date).calendar();

export const formatDate = {
    toShortString,
    toDayString,
    toLongString,
    fromNow
};

export const UNKNOWN_OPERATION_TYPE = 'type.unknown';
export const UNKNOWN_ACCOUNT_TYPE = 'account-type.unknown';

export const MIN_WEBOOB_VERSION = '1.5';

// At least 8 chars, including one lowercase, one uppercase and one digit.
const PASSPHRASE_VALIDATION_REGEXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

export function validatePassword(password) {
    return PASSPHRASE_VALIDATION_REGEXP.test(password);
}

const DEFERRED_CARD_TYPE = OPERATION_TYPES.find(type => type.name === 'type.deferred_card');
const SUMMARY_CARD_TYPE = OPERATION_TYPES.find(type => type.name === 'type.card_summary');
const ACCOUNT_TYPE_CARD = ACCOUNT_TYPES.find(type => type.name === 'account-type.card');

export const shouldIncludeInBalance = (op, balanceDate, accountType) => {
    let opDebitMoment = moment(op.debitDate || op.date);
    return (
        opDebitMoment.isSameOrBefore(balanceDate, 'day') &&
        (op.type !== DEFERRED_CARD_TYPE.name || accountType === ACCOUNT_TYPE_CARD.name)
    );
};

export const shouldIncludeInOutstandingSum = op => {
    let opDebitMoment = moment(op.debitDate || op.date);
    let today = moment();
    return opDebitMoment.isAfter(today, 'day') && op.type !== SUMMARY_CARD_TYPE.name;
};

export const FETCH_STATUS_SUCCESS = 'OK';
